// server.js
'use strict';
const http = require('http');

const server = http.createServer(function (req, res) {
    res.writeHead(200, {'content-type': 'text/plain'});
    res.end('Hola Mundo 201807228');
});
server.listen(3000);